//Database Activity

db.users.insertMany([
		{
			"firstName": "Edward",
			"lastName": "Elric",
			"email": "edwardelric@mail.com",
			"password": "iluvwinry",
			"isAdmin": false
		},
		{
			"firstName": "Alphonse",
			"lastName": "Elric",
			"email": "alphelric@mail.com",
			"password": "metal4ever",
			"isAdmin": false
		},
		{
			"firstName": "Winry",
			"lastName": "Rockbell",
			"email": "winryheart143@mail.com",
			"password": "iluvedward",
			"isAdmin": false
		},
		{
			"firstName": "Riza",
			"lastName": "Hawkeye",
			"email": "rizaXroy@mail.com",
			"password": "iluvmustang",
			"isAdmin": false
		},
		{
			"firstName": "Roy",
			"lastName": "Mustang",
			"email": "roymustang@mail.com",
			"password": "iluvfire",
			"isAdmin": false
		}

])

db.courses.insertMany([
			{
				"name": "Alchemy 100",
				"price": 2500,
				"isActive": false
			},
			{
				"name": "Philosophy 300-A",
				"price": 3500,
				"isActive": false
			},
			{
				"name": "Military Leadership 100-A",
				"price": 5000,
				"isActive": false
			}

	])

//Find non-admin users
db.users.find({"isAdmin" : false})

//Update the first user in user collection as admin
db.users.updateOne({"firstName" : "Edward"}, {$set: {"isAdmin" : true}})

//Update one of the courses as active
db.courses.updateOne({"name" : "Philosophy 300-A"}, {$set: {"isActive" : true}})

//Delete all inactive courses
db.courses.deleteMany({"isActive" : false})





